var express = require('express');
app=express(),
port=process.env.PORT||3000;

var path=require('path');
var cors = require('cors');
app.use(cors());
app.use(express.static(__dirname+'/build/default'));
app.listen(port);

console.log("Proyecto polymer ejecutandose en el puerto 3000");

app.get('/',(req,res)=>{
  res.sendFile("index.html",{root:'.'});
})
